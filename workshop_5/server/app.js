const express = require("express");
const bodyParser = require("body-parser");
const graphqlExpress = require("graphql-server-express").graphqlExpress;
const graphiqlExpress = require("graphql-server-express").graphiqlExpress;
const schema = require("./graphql-schema.js");
const cors = require("cors");

const homePath = "/graphiql";
const URL = "http://localhost";
const PORT = 4000;

// instance the expressJS app
const app = express();

app.use(cors());

try {
  app.use("/graphql", bodyParser.json(), graphqlExpress({ schema }));

  //one single endpoint different than REST
  app.use(
    homePath,
    graphiqlExpress({
      endpointURL: "/graphql",
    })
  );

  app.listen(PORT, () => {
    console.log(`Visit ${URL}:${PORT}${homePath}`);
  });
} catch (e) {
  console.log(e);
}
