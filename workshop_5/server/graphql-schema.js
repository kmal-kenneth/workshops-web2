const ObjectId = require("mongodb").ObjectId;
const makeExecutableSchema = require("graphql-tools").makeExecutableSchema;
const prepare = require("./util/index").prepare;

const { db, Posts, Comments } = require("./db.js");

const typeDefs = [
  `
    type Query {
      post(_id: String): Post
      posts: [Post]
      comment(_id: String): Comment
    }

    type Post {
      _id: String
      title: String
      content: String
      comments: [Comment]
    }

    type Comment {
      _id: String
      postId: String
      content: String
      post: Post
    }

    type Mutation {
      createPost(title: String, content: String): Post
      createComment(postId: String, content: String): Comment
    }

    schema {
      query: Query
      mutation: Mutation
    }
  `,
];

const resolvers = {
  Query: {
    post: async (root, { _id }) => {
      return prepare(await Posts.findOne(ObjectId(_id)));
    },
    posts: async () => {
      return (await Posts.find({}).toArray()).map(prepare);
    },
    comment: async (root, { _id }) => {
      return prepare(await Comments.findOne(ObjectId(_id)));
    },
  },
  Post: {
    comments: async ({ _id }) => {
      return (await Comments.find({ postId: _id }).toArray()).map(prepare);
    },
  },
  Comment: {
    post: async ({ postId }) => {
      return prepare(await Posts.findOne(ObjectId(postId)));
    },
  },
  Mutation: {
    createPost: async (root, args, context, info) => {
      const res = await Posts.insertOne(args);
      return prepare(res.ops[0]); // https://mongodb.github.io/node-mongodb-native/3.1/api/Collection.html#~insertOneWriteOpResult
    },
    createComment: async (root, args) => {
      const res = await Comments.insert(args);
      return prepare(await Comments.findOne({ _id: res.insertedIds[1] }));
    },
  },
};

const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
});

module.exports = schema;
