// database connection
const mongoose = require("mongoose");

const MONGO_URL = "mongodb://127.0.0.1:27017/week5";

mongoose.connect(
  MONGO_URL,
  { useNewUrlParser: true, useUnifiedTopology: true },
  (err) => {
    if (err) console.error(err);
    else console.log("Connected to the mongodb");
  }
);

var db = mongoose.connection;

const Posts = db.collection("posts");
const Comments = db.collection("comments");

module.exports = {
  db,
  Posts,
  Comments,
};
