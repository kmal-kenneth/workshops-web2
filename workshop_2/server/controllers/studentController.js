const Student = require("../models/studentsModel");

/**
 * Creates a student
 *
 * @param {*} req
 * @param {*} res
 */
const studentPost = (req, res) => {
  var student = new Student();

  student.firstname = req.body.firstname;
  student.lastname = req.body.lastname;
  student.email = req.body.email;
  student.address = req.body.address;

  if (student.firstname && student.lastname) {
    student.save(function (err) {
      if (err) {
        res.status(422);
        console.log("error while saving the student", err);
        res.json({
          error: "There was an error saving the student",
        });
      }
      res.status(201); //CREATED
      res.header({
        location: `http://localhost:3000/api/students/?id=${student.id}`,
      });
      res.json(student);
    });
  } else {
    res.status(422);
    console.log("error while saving the student");
    res.json({
      error: "No valid data provided for student",
    });
  }
};

/**
 * Get all students
 *
 * @param {*} req
 * @param {*} res
 */
const studentGet = (req, res) => {
  // if an specific student is required
  if (req.query && req.query.id) {
    Student.findById(req.query.id, function (err, student) {
      if (err) {
        res.status(404);
        console.log("error while queryting the student", err);
        res.json({ error: "student doesnt exist" });
      }
      res.json(student);
    });
  } else {
    // get all students
    Student.find(function (err, students) {
      if (err) {
        res.status(422);
        res.json({ error: err });
      }
      res.json(students);
    });
  }
};

/**
 * Updates a student
 *
 * @param {*} req
 * @param {*} res
 */
const studentPatch = (req, res) => {
  // get student by id
  if (req.query && req.query.id) {
    Student.findById(req.query.id, function (err, student) {
      if (err) {
        res.status(404);
        console.log("error while queryting the student", err);
        res.json({ error: "student doesnt exist" });
      }

      // update the student object (patch)
      student.firstname = req.body.firstname
        ? req.body.firstname
        : student.firstname;
      student.lastname = req.body.lastname
        ? req.body.lastname
        : student.lastname;
      student.email = req.body.email ? req.body.email : student.email;
      student.address = req.body.address ? req.body.address : student.address;

      student.save(function (err) {
        if (err) {
          res.status(422);
          console.log("error while saving the student", err);
          res.json({
            error: "There was an error saving the student",
          });
        }
        res.status(200); // OK
        res.json(student);
      });
    });
  } else {
    res.status(404);
    res.json({ error: "student doesnt exist" });
  }
};

/**
 * Delete a student
 *
 * @param {*} req
 * @param {*} res
 */
const studentDelete = (req, res) => {
  // get student by id
  if (req.query && req.query.id) {
    Student.findById(req.query.id, function (err, student) {
      if (err) {
        res.status(404);
        console.log("error while queryting the student", err);
        res.json({ error: "student doesnt exist" });
      }

      student.remove(
        {
          _id: req.params.id,
        },
        function (err, contact) {
          if (err) {
            res.status(422);
            console.log("error while deleting the student", err);
            res.json({
              error: "There was an error deleting the student",
            });
          }
          res.status(204); // OK
          res.json({
            message: "student deleted",
          });
        }
      );
    });
  } else {
    res.status(422);
    res.json({ error: "Requires student id." });
  }
};
module.exports = {
  studentGet,
  studentPost,
  studentPatch,
  studentDelete,
};
