const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const product = new Schema({
  name: String,
  price: Schema.Types.Decimal128,
  quantity: Number,
});

module.exports = mongoose.model("Product", product);
