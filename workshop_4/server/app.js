const express = require("express");
const graphqlHTTP = require("express-graphql");

// database connection
const mongoose = require("mongoose");

mongoose.connect(
  "mongodb://127.0.0.1:27017/week4",
  { useNewUrlParser: true, useUnifiedTopology: true },
  (err) => {
    if (err) console.error(err);
    else console.log("Connected to the mongodb");
  }
);

var db = mongoose.connection;

// instance the expressJS app
const app = express();

const schema = require("./graphql-schema.js");

//one single endpoint different than REST
app.use(
  "/graphql",
  graphqlHTTP({
    schema: schema,
    graphiql: true,
  })
);

app.listen(4000, () => console.log("Now browse to localhost:4000/graphql"));
