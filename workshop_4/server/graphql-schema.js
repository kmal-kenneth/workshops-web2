const graphql = require("graphql");

const Client = require("./models/client_model.js");
const Product = require("./models/product_model.js");
const Order = require("./models/order_model.js");

const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLID,
  GraphQLInt,
  GraphQLSchema,
  GraphQLList,
  GraphQLNonNull,
} = graphql;

const ClientType = new GraphQLObjectType({
  name: "client",
  fields: () => ({
    id: { type: GraphQLID },
    name: { type: GraphQLString },
    lastName: { type: GraphQLString },
    email: { type: GraphQLString },
    website: { type: GraphQLString },
    orders: {
      type: new GraphQLList(OrderType),
      resolve(parent, args) {
        return Order.find({ clientID: parent.id });
      },
    },
  }),
});

const ProductType = new GraphQLObjectType({
  name: "Product",
  fields: () => ({
    id: { type: GraphQLID },
    name: { type: GraphQLString },
    quantity: { type: GraphQLInt },
    orders: {
      type: new GraphQLList(OrderType),
      resolve(parent, args) {
        return Order.find({ productsID: parent.id });
      },
    },
  }),
});

const OrderType = new GraphQLObjectType({
  name: "Order",
  fields: () => ({
    id: { type: GraphQLID },
    client: {
      type: ClientType,
      resolve(parent, args) {
        return Client.findById(parent.clientID);
      },
    },
    products: {
      type: new GraphQLList(ProductType),
      resolve(parent, args) {
        let productsTemp = [];
        parent.productsID.forEach((id) => {
          productsTemp.push(Product.findById(id));
        });

        return productsTemp;
      },
    },
  }),
});

const RootQuery = new GraphQLObjectType({
  name: "RootQueryType",
  fields: {
    client: {
      type: ClientType,
      args: { id: { type: GraphQLID } },
      resolve(parent, args) {
        return Client.findById(args.id);
      },
    },
    clients: {
      type: new GraphQLList(ClientType),
      resolve(parent, args) {
        return Client.find({});
      },
    },
    products: {
      type: new GraphQLList(ProductType),
      resolve(parent, args) {
        return Product.find({});
      },
    },
    product: {
      type: ProductType,
      args: { id: { type: GraphQLID } },
      resolve(parent, args) {
        return Product.findById(args.id);
      },
    },
    orders: {
      type: new GraphQLList(OrderType),
      resolve(parent, args) {
        return Order.find({});
      },
    },
    order: {
      type: OrderType,
      args: { id: { type: GraphQLID } },
      resolve(parent, args) {
        return Order.findById(args.id);
      },
    },
  },
});

const Mutation = new GraphQLObjectType({
  name: "Mutation",
  fields: {
    addClient: {
      type: ClientType,
      args: {
        //GraphQLNonNull make these field required
        name: { type: new GraphQLNonNull(GraphQLString) },
        lastName: { type: new GraphQLNonNull(GraphQLString) },
      },
      resolve(parent, args) {
        let client = new Client({
          name: args.name,
          lastName: args.lastName,
        });
        return client.save();
      },
    },
    addProduct: {
      type: ProductType,
      args: {
        //GraphQLNonNull make these field required
        name: { type: new GraphQLNonNull(GraphQLString) },
        quantity: { type: new GraphQLNonNull(GraphQLInt) },
        // price: { type: new GraphQLNonNull(GraphQLInt) },
      },
      resolve(parent, args) {
        let product = new Product({
          name: args.name,
          quantity: args.quantity,
          // price: args.price,
        });
        return product.save();
      },
    },
    addOrder: {
      type: OrderType,
      args: {
        //GraphQLNonNull make these field required
        clientID: { type: new GraphQLNonNull(GraphQLID) },
        productsID: { type: new GraphQLNonNull(GraphQLList(GraphQLID)) },
      },
      resolve(parent, args) {
        let order = new Order({
          clientID: args.clientID,
          productsID: args.productsID,
        });
        return order.save();
      },
    },
  },
});

module.exports = new GraphQLSchema({
  query: RootQuery,
  mutation: Mutation,
});
